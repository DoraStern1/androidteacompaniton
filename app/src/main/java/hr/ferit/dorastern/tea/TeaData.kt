package hr.ferit.dorastern.tea

data class TeaData(

    var id: String? = null,

    var imageUrl: String? = "https://cdn.pixabay.com/photo/2013/07/12/18/21/tea-153336_1280.png",
    var name: String? = null,
    var description: String? = "No description",
    var type: String?=null,
    var comment: String?= "No comment",
    var rating: Double? = null,
    var date: String?="Not defined",


)
