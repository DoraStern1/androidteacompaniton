package hr.ferit.dorastern.tea

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore


class SecondFragment : Fragment(){

    lateinit var db: FirebaseFirestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        db= FirebaseFirestore.getInstance()

        val teaID= arguments?.getString("id")



        val view =inflater.inflate(R.layout.fragment_second, container, false)


       var teaImage= view.findViewById<ImageView>(R.id.imageDetail)

        val teaName= view.findViewById<TextView>(R.id.NameDetail)
        val teaType= view.findViewById<TextView>(R.id.typeDetail)
        val teaDescription= view.findViewById<TextView>(R.id.descriptionDetail)
        val teaComment= view.findViewById<TextView>(R.id.commentDetail)
       val teaRating= view.findViewById<RatingBar>(R.id.ratingDetail)
        val teaDate= view.findViewById<TextView>(R.id.dateDetail)

        val menuButton= view.findViewById<ImageButton>(R.id.saveButton)
        teaRating.setIsIndicator(true)



        if(teaID != null){
            db.collection("TeaDetails").document(teaID).get().addOnSuccessListener { Tea ->

                if(Tea.exists()){
                    val tea= Tea.toObject(TeaData::class.java)

                    Glide.with(this)
                        .load(tea?.imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(teaImage)

                    teaName.setText(tea?.name.toString())
                    teaType.setText(tea?.type.toString())
                    teaDescription.setText(tea?.description.toString())
                    teaComment.setText(tea?.comment.toString())
                    teaRating.rating = tea?.rating?.toFloat() ?: 0.0f
                    teaDate.setText(tea?.date.toString())
                }
            }

        }

        view.findViewById<ImageButton>(R.id.backButton).setOnClickListener{
            val fragmentTransaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(this.id, MainFragment())
            fragmentTransaction?.commit()
        }

        val popup = PopupMenu(requireContext(), menuButton)
        popup.inflate(R.menu.basic_menu)


        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.editOption->{

                    val bundle= Bundle()
                    bundle.putString("id", teaID)

                    val fragment = FirstFragment()
                    fragment.arguments= bundle

                    val fragmentTransaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(this.id, fragment)
                    fragmentTransaction?.commit()

                    true
                }
                R.id.deleteOption -> {
                    db.collection("TeaDetails").document(teaID!!).delete()
                        .addOnSuccessListener {
                            Log.d("YourAdapter", "Item deleted from Firestore")
                        }
                        .addOnFailureListener { e ->
                            Log.w("YourAdapter", "Error deleting item from Firestore", e)
                        }
                    val mainFragment= MainFragment()
                    val fragmentTransaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(this.id, mainFragment)
                    fragmentTransaction?.commit()

                    true
                }
                else -> false
            }
        }

        menuButton.setOnClickListener{
            popup.show()
        }

        /*
        Glide.with(this)
            .load("https://www.example.com/image.jpg")
            .error(R.drawable.ic_error)
            .placeholder(R.drawable.ic_placeholder)
            .into()*/

        return view

    }


}