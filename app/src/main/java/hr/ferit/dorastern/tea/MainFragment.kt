package hr.ferit.dorastern.tea

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.PopupMenu
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore

class MainFragment : Fragment() {
    private  lateinit var teaList: RecyclerView
    private lateinit var oglist: ArrayList<TeaData>
   private  lateinit var db : FirebaseFirestore

    private lateinit var myadapter: TeaAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        oglist = arrayListOf()
        val view=inflater.inflate(R.layout.fragment_main, container, false)

        db= FirebaseFirestore.getInstance()


        val searchButton= view.findViewById<ImageButton>(R.id.searchButton)
        val editFragment= FirstFragment()

        val addButton= view.findViewById<FloatingActionButton>(R.id.addTeaButton)

        teaList = view.findViewById(R.id.teaList)

        val filterButton= view.findViewById<ImageButton>(R.id.saveButton)
        val xButton= view.findViewById<ImageButton>(R.id.xButton)

        val popup = PopupMenu(requireContext(), filterButton)
        popup.inflate(R.menu.filter_menu_options)

        filterButton.setOnClickListener{ popup.show() }


        val searchText= view.findViewById<EditText>(R.id.searchText)

        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()

        db.collection("TeaDetails").get().addOnSuccessListener {
                result ->
            oglist= ArrayList()
            for(data in result.documents)
            {
                val tea= data.toObject(TeaData::class.java)
                if(tea != null){
                    tea.id= data.id
                    oglist.add(tea)
                }
            }
            myadapter=TeaAdapter(oglist, fragmentTransaction)

            teaList.adapter= myadapter

        }

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.option1 -> {
                    oglist.sortBy { it.name?.lowercase()  }
                    myadapter.refresh()
                    true
                }
                R.id.option2 -> {
                    oglist.sortBy { it.type?.lowercase() }
                    myadapter.refresh()
                    true
                }
                R.id.option3 -> {
                    oglist.sortBy { it.rating }
                    myadapter.refresh()
                    true
                }
                else ->
                    false
            }
        }


        addButton?.setOnClickListener{
            fragmentTransaction?.replace(R.id.fragmentMain, editFragment)
            fragmentTransaction?.commit()
        }

        teaList.layoutManager = LinearLayoutManager(this.context)
        teaList.setHasFixedSize(true)

            xButton.setOnClickListener{
                searchText.setText("")
                myadapter.restoreList()

        }
        searchButton?.setOnClickListener{
            if(searchText.text.isNotEmpty()) {
                myadapter.filterItems(searchText.text.toString())
            }
            else{
                myadapter.restoreList()
            }
        }


        return view
    }
}

