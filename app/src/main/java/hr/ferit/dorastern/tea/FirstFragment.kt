package hr.ferit.dorastern.tea


import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*


class FirstFragment : Fragment() {

    private lateinit var dateText: TextView

    private val dateFormat = SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault())


    lateinit var db: FirebaseFirestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_first, container, false)

        var teaID= arguments?.getString("id")

        var newId=teaID
        val bundle=Bundle()
        db= FirebaseFirestore.getInstance()

        val imageView=view.findViewById<ImageView>(R.id.imageDetail)
        val imageUrl= view.findViewById<EditText>(R.id.imageSetup)
        val teaName= view.findViewById<EditText>(R.id.nameSetup)
        val teaType= view.findViewById<EditText>(R.id.typeSetup)
        val teaDescription=  view.findViewById<EditText>(R.id.descriptionSetup)
        val teaComment= view.findViewById<EditText>(R.id.commentSetup)
        val teaRating= view.findViewById<RatingBar>(R.id.ratingDetail)
        val dateBtn=view.findViewById<Button>(R.id.dateSetup)

         dateText= view.findViewById(R.id.DateAdded)

        fun deleteFromEditText(){
          view.findViewById<ImageButton>(R.id.xName).setOnClickListener{
                if(teaName.text.isNotEmpty()){
                    teaName.setText("")

                }

            }
            view.findViewById<ImageButton>(R.id.xComment).setOnClickListener{
                if(teaComment.text.isNotEmpty())
                    teaComment.setText("")
            }
            view.findViewById<ImageButton>(R.id.xUrl).setOnClickListener{
                if(imageUrl.text.isNotEmpty())
                    imageUrl.setText("")
            }
            view.findViewById<ImageButton>(R.id.xDescription).setOnClickListener{
                if(teaDescription.text.isNotEmpty())
                    teaDescription.setText("")
            }
        }

        deleteFromEditText()

        val popup = PopupMenu(requireContext(), teaType)
        popup.inflate(R.menu.tea_type_menu)

//set tea type
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.option1 -> {
                    teaType.setText(item.title.toString())
                    true

                }
                R.id.option2 -> {
                    teaType.setText(item.title.toString())
                    true
                }
                R.id.option3 -> {
                    teaType.setText(item.title.toString())
                    true

                }
                R.id.option4 -> {
                    teaType.setText(item.title.toString())
                    true
                }
                R.id.option5 -> {
                    teaType.setText(item.title.toString())
                    true

                }
                R.id.option6 -> {
                    teaType.setText(item.title.toString())
                    true
                }
                R.id.option7 -> {
                    teaType.setText(item.title.toString())
                    true

                }

                else -> false

            }
        }

        teaType.setOnClickListener{
            popup.show()
        }

//set date
        dateBtn.setOnClickListener {
            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(requireContext(),
                DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                    val selectedDate = Calendar.getInstance()
                    selectedDate.set(selectedYear, selectedMonth, selectedDay)
                    dateText.text = dateFormat.format(selectedDate.time)
                }, year, month, day)


            datePickerDialog.show()
        }

        if(teaID != null){
            db.collection("TeaDetails").document(teaID).get().addOnSuccessListener { Tea ->

                if(Tea.exists()){
                    val tea= Tea.toObject(TeaData::class.java)

                    teaName.setText(tea?.name.toString())
                    teaType.setText(tea?.type.toString())
                    teaDescription.setText(tea?.description.toString())
                    teaComment.setText(tea?.comment.toString())
                    teaRating.rating = tea?.rating?.toFloat() ?: 0.0f
                    dateText.text = tea?.date.toString()
                    imageUrl.setText(tea?.imageUrl.toString())

                    Glide.with(this)
                        .load(tea?.imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(imageView)
                }

            }
        }
        fun getTea(): TeaData{
            val tea=TeaData()

            tea.imageUrl= imageUrl.text.toString()
            tea.name=teaName.text.toString()
            tea.type=teaType.text.toString()
            tea.description=teaDescription.text.toString()
            tea.comment= teaComment.text.toString()
            tea.rating= teaRating.rating.toDouble()
            tea.date= dateText.text.toString()

            return tea
        }

        fun saveTea(){

            val tea= getTea()

            val teaMap= hashMapOf(
                "name" to tea.name,
                "type" to tea.type,
                "description" to tea?.description,
                "comment" to tea?.comment,
                "imageUrl" to tea.imageUrl,
                "rating" to tea.rating,
                "date" to tea?.date,
                )

            if (teaID != null){
                db.collection("TeaDetails").document(teaID)
                    .set(teaMap)
                    .addOnSuccessListener {

                        Log.d("tag","DocumentSnapshot successfully written!") }
                    .addOnFailureListener{e -> Log.w("tag", "Error writing document", e)}
            }
            else{
                db.collection("TeaDetails").add(teaMap)
                    .addOnSuccessListener{
                    }
            }
        }

        val nextButton = view.findViewById<ImageButton>(R.id.saveButton)

        nextButton.setOnClickListener {



        if(teaName.text.isNotEmpty()) {

            saveTea()

        val fragmentTransaction: FragmentTransaction? =
        activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(this.id, MainFragment())
        fragmentTransaction?.commit()
}
            else{
                Toast.makeText(requireContext(), "Please fill in the name field", Toast.LENGTH_SHORT).show()
            }
        }

        val backButton= view.findViewById<ImageButton>(R.id.backButton)
        backButton.setOnClickListener{

            val mainFragment= MainFragment()
            val fragmentTransaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(this.id, mainFragment)
            fragmentTransaction?.commit()
        }
        return view
    }
}