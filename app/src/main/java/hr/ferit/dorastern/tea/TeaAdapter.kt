package hr.ferit.dorastern.tea

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.collections.ArrayList



class TeaAdapter (var items: ArrayList<TeaData>, val fragmentTransaction: FragmentTransaction?): RecyclerView.Adapter<TeaAdapter.MyViewHolder>(){

    private  lateinit var db : FirebaseFirestore
    private val originalList = ArrayList(items)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        db= FirebaseFirestore.getInstance()
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem:TeaData= items[position]

        holder.teaName.setText(currentItem.name)
        holder.teaType.setText(currentItem.type)
        holder.teaDescription.setText(currentItem.description)
        holder.teaRating.rating=currentItem.rating?.toFloat()?:0.0f
        holder.teaDate.setText(currentItem?.date)

        Glide.with(holder.teaImage.context)
            .load(items[position].imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(holder.teaImage)



        val popup = PopupMenu(holder.menuButton.context, holder.menuButton)
        popup.inflate(R.menu.basic_menu)


        holder.menuButton.setOnClickListener{ popup.show()
        }

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.editOption-> {
                    val sendBundle= Bundle()
                    sendBundle.putString("id", currentItem.id)
                    holder.editFragment.arguments=sendBundle
                    fragmentTransaction?.replace(R.id.fragmentMain,holder.editFragment)
                    fragmentTransaction?.commit()
                    true
                }
                R.id.deleteOption -> {
                    val documentId = items[position].id

                    items.removeAt(position)
                    notifyItemRemoved(position)


                    db.collection("TeaDetails").document(documentId!!).delete()
                        .addOnSuccessListener {
                            notifyDataSetChanged()
                            Log.d("YourAdapter", "Item deleted from Firestore")

                        }
                        .addOnFailureListener { e ->
                            Log.w("YourAdapter", "Error deleting item from Firestore", e)
                        }
                    true
                }
                else -> false
            }
        }


        holder.itemView.setOnClickListener {
            val sendBundle= Bundle()
            sendBundle.putString("id", currentItem.id)
            holder.detailFragment.arguments=sendBundle
            fragmentTransaction?.replace(R.id.fragmentMain,holder.detailFragment)
            fragmentTransaction?.commit()
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun filterItems(query: String) {
    Log.d("message test items",items.toString() )

        val filteredList = items.filter {
            it.name?.contains(query, true) ?: false || it.type?.contains(query, true)?: false
        }
        items.clear()
        items.addAll(filteredList)
        notifyDataSetChanged()
    }


    fun restoreList() {
        items.clear()
        items.addAll(originalList)
        notifyDataSetChanged()
    }

    fun refresh(){
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         val teaImage= itemView.findViewById<ImageView>(R.id.teaImage)
        val teaDate= itemView.findViewById<TextView>(R.id.teaDate)
         val teaName=itemView.findViewById<TextView>(R.id.teaName)
         val teaType= itemView.findViewById<TextView>(R.id.teaType)
         val teaDescription= itemView.findViewById<TextView>(R.id.teaDescription)
            val teaRating= itemView.findViewById<RatingBar>(R.id.ratingBar)

        val id: String?="null"

        val editFragment= FirstFragment()
        val detailFragment= SecondFragment()
        val menuButton= itemView.findViewById<ImageButton>(R.id.menuButton)


    }



}

